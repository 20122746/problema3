import os
from typing import Dict, Any, Union, List


def dict_files(root_folder: str = ".\\data") -> Dict[str, Union[int, any]]:
    result = {}
    temp = []
    print("ler ficheiros da pasta "+root_folder)
    """ determinar o conjunto de ficheiros a ler"""
    for root, directories, files in os.walk(root_folder):
        for name in files:
            temp.append(os.path.join(root, name))

            filename = os.path.join(root, name)
            # print(filename)

            """ analisar cada ficheiro"""
            with open(filename, 'r') as ficheiro:
                for linha in ficheiro:
                    for palavra in linha.split():
                        if len(palavra) > 3:
                            if palavra in result:
                                result[palavra] = result[palavra] + 1
                            else:
                                result[palavra] = 1
                        else:
                            print(palavra)
    return result


def stat_dict(dictio: Dict[str, Union[int, any]], nr_elem: int) -> Dict[str, Union[float, any]]:
    result = {}
    # print(total)
    """ calcular a probabilidade de cada palavra existir no dicionário"""
    for key in dictio:
        result[key] = dictio[key] / nr_elem
        # print(key + " - " + str(result[key]))
    return result


""" ler um ficheiro para testar o seu conteudo"""


def read_testfile(_path: str, _file: str) -> list:
    result = []
    filename = os.path.join(_path, _file)
    # verificar se o ficheiro existe
    if os.path.isfile(filename):
        with open(filename, 'r') as ficheiro:
            for linha in ficheiro:
                for palavra in linha.split():
                    result.append(palavra)
    # se o ficheiro não existir, retorna uma lista vazia
    return result

# foram acrescentados os parametros num_words_spam e num_words_nonspam


def test_file(words:list, pspam_dict:dict, pnonspam_dict: dict, num_words_spam=0, num_words_nonspam=0) -> int:
    v_spam = 1.0
    v_nao_spam = 1.0

    for palavra in words:
        if palavra in pspam_dict:
            v_spam = v_spam*pspam_dict[palavra]
            if palavra in pnonspam_dict:
                v_nao_spam = v_nao_spam*(1/num_words_nonspam)
        if palavra in pnonspam_dict:
            v_nao_spam = v_nao_spam*pnonspam_dict[palavra]
            if palavra in pspam_dict:
                v_spam = v_spam*(1/num_words_spam)

    if v_spam > v_nao_spam:
        result = 1
    else:
        result = 0

    return result


def main():
    dict_spam = dict_files(".\\data\\spam-train")
    prob_spam = stat_dict(dict_spam, sum(dict_spam.values()))
    dict_nonspam = dict_files(".\\data\\nonspam-train")
    prob_nonspam = stat_dict(dict_nonspam, sum(dict_spam.values()))

    print(prob_spam)
    print(prob_nonspam)

    # considerar que o ficheiro a analizar está numa sub-pasta na raiz do projeto
    words_to_test = read_testfile('data\\spam-test', 'spmsga163.txt')
    if words_to_test:
        print(words_to_test)
        if test_file(words_to_test,prob_spam,prob_nonspam, sum(dict_spam.values()), sum(dict_nonspam.values())) == 0:
            print("Não spam")
        else:
            print('É spam')
    else:
        print("Erro na abertura do ficheiro\n")


main()
